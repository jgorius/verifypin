# Modèle de l'attaquant

Une attaque par observation mesure l'environnent physique du circuit pour essayer de gagner de l'information sur le calcul qui s'y déroule. Il s'agit souvent de mesurer la consommation de courant ou le rayonnement électromagnétique.

Dans cet exercice, la fuite d'information sera simulée : l'attaquant est capable de récupérer la somme de tous les registres internes de la puce pour chaque instruction de *verify_pin* et *compare_arrays* (la somme, pas les valeurs individuellement).

## Attaque par apprentissage

Nous allons utiliser un réseau de neurones simples pour apprendre le lien entre les traces mesurées et le PIN secret présent dans la carte. Ainsi l'attaque présuppose que l'attaquant dispose d'une puce identique à la cible sur laquelle il peut mesurer ce qu'il veut. En particulier, il est capable de changer le PIN secret sur sa carte et de faire autant d'essais qu'il le veut (il suffit de rentrer le bon PIN toutes les 2 mesures).

## Environnement de travail

## Avec Nix

Utilisez la configuration Nix du répertoire L4 :

```bash
nix-shell ./L4/default.nix
```

### Installation des paquets nécessaires (mode manuel)

Les paquets python keras et tensorflow doivent être installés pour cet exercice.

```
pip3 install tensorflow
pip3 install keras
```

## Lancer l'attaque

Cette attaque nécessite beaucoup de ressources, un PC puissant est recommandé, la simulation de suffisamment de vérification de code PIN peut être longue (entre 30min et des heures...).

La première exécution lancera l'apprentissage.
```
python3 ../L4/H-deep-learning.py .
```

Pour recharger l'apprentissage précedent, utiliser `--load`.

```
python3 ../L4/H-deep-learning.py . --load
```

## Contremesure

Mais alors comment se protéger d'une telle attaque ?
