/*
 * File: pin.c
 * Project: target_app
 * Created Date: Monday May 25th 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st July 2020 10:54:38 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */

#include "pin.h"
#include "nvm.h"

//this is the secret pin value
const uint8_t secret_pin[4] = {3, 1, 4, 1};


bool compare_arrays(const uint8_t* a, const uint8_t* b, size_t len) {
    return false; // Remplacer par votre propre fonction
}

int verify_pin(const uint8_t* to_verify, size_t len) {
    return INVALID; // Remplacer par votre propre fonction
}